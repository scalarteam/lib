// use std::fs::File;
// use std::io::prelude::*;

// mod str;
// mod vector;
// #[macro_use]
// extern crate serde_derive;

// fn main() {

//     let filename = "./example-document.txt";
//     let mut f = File::open(filename).expect("file not found");

//     let mut contents = String::new();
//     f.read_to_string(&mut contents)
//         .expect("something went wrong reading the file");


//     // let test_string = String::from("Partitioning and parallelism are touchstones for major relational database systems. Proprietary database vendors manage to extract a premium from a minority of users by upselling features in these areas. While PostgreSQL has had some of these \"high-tier\" items for many years (e.g., CREATE INDEX CONCURRENTLY, advanced replication functionality), the upcoming release expands the number considerably. I may be biased as a PostgreSQL major contributor and committer, but it seems to me that the belief that community-run database system projects are not competitive with their proprietary cousins when it comes to scaling enterprise workloads has become just about untenable.");
//     // let cleaned_string = str::clean_string(&test_string);
//     let term_frequency_map = str::n_gram_frequency_map_from_string(3, &contents);
//     let idf_map = str::get_idf_from_file("idf.csv");
//     let tf_idf_map = str::get_tf_idf_map(&term_frequency_map, &idf_map);
//     let max_info_loss = 0.01_f64;
//     println!("Reducing to {}% information", (1_f64 - max_info_loss) * 100_f64);
//     let reduced_dimension_map = vector::reduce_dimension(&tf_idf_map, max_info_loss);
//     let normed_tf_idf_map = vector::normalize(&reduced_dimension_map);

//     let term_iterator = normed_tf_idf_map.keys();
//     let mut terms: Vec<String> = Vec::new();
//     for term in term_iterator {
//         terms.push(term.to_string());
//     }

//     terms.sort_by(|a, b| normed_tf_idf_map[a].partial_cmp(&normed_tf_idf_map[b]).unwrap());
    
//     for term in terms {
//         println!("{}: {}", term, normed_tf_idf_map[&term])
//     }

//     println!("{}", vector::dot(&normed_tf_idf_map, &normed_tf_idf_map))
// }