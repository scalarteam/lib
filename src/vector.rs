use std::cmp::Reverse;
use std::collections::HashMap;
use std::iter::FromIterator;

/// Given a vector, return a normed vector
pub fn normalize(vector: &HashMap<String, f64>) -> HashMap<String, f64> {
    let mut normed_vector: HashMap<String, f64> = HashMap::new();
    let norm = length(vector);

    for dimension in vector.keys() {
        normed_vector.insert(dimension.to_string(), vector[dimension] / norm);
    }

    normed_vector
}

/// Find the length (norm) of a vector
pub fn length(vector: &HashMap<String, f64>) -> f64 {
    vector.values().fold(0_f64, |acc, idf| acc + idf.powf(2.0_f64)).sqrt()
}

/// Given an N-dimensional vector, reduce dimensionality as much as possible
/// while maintaining at least [max_information_loss]% of the original
/// information in the vector.
/// 
/// There are a billion algorithms that are more efficient than this one,
/// but this one was super easy to implement.
pub fn reduce_dimension(vector: &HashMap<String, f64>, max_information_loss: f64) -> HashMap<String, f64> {
    let original_vector_norm = length(vector);

    let mut values: Vec<f64> = Vec::new();
    for value in vector.values() {
        values.push(value.clone());
    }
    values.sort_by_key(|&num| Reverse((num * 1_000_000_f64) as u64));

    let mut new_values: Vec<f64> = Vec::new();

    for value in values {
        let iterator = new_values.clone().into_iter();
        let information_loss = original_vector_norm - iterator.fold(0_f64, |acc, idf| acc + idf.powf(2.0_f64)).sqrt();

        if information_loss < (max_information_loss * original_vector_norm) {
            break;
        } else {
            new_values.push(value);
        }
    };

    let number_of_dimensions = new_values.len();
    let mut new_vector: HashMap<String, f64> = HashMap::new();

    let mut term_tfidf_tuples = Vec::from_iter(vector);
    term_tfidf_tuples.sort_by_key(|&(_, a)| Reverse((a * 1_000_000_f64) as u64));

    for (term, tfidf) in &term_tfidf_tuples[0..number_of_dimensions] {
        new_vector.insert(term.to_string(), *tfidf.clone());
    }

    new_vector
}

/// Calculate the dot product of a and b
pub fn dot(a: &HashMap<String, f64>, b: &HashMap<String, f64>) -> f64 {
    let mut product = 0_f64;

    for term in a.keys() {
        if !b.contains_key(term) {
            continue;
        }

        product += a[term] * b[term];
    }
    
    product
}