extern crate csv;
extern crate ngrams;
extern crate unidecode;
use std::fs;
use std::collections::HashMap;
use str::ngrams::Ngram;

/// "Clean" the input string so that it can be used
/// for text analysis. Cleaning means removing punctuation and
/// transliterating non-ASCII strings into ASCII strings.
/// 
/// This is done so that term frequency analysis can
/// be done on documents from any language.
/// 
/// Note for non-English documents:
/// In reality, we would also need an IDF for more than
/// just the American English corpus, and we would need
/// some way of determining which corpus the input string
/// should be checked against. This function is written in
/// anticipation of that happening.
pub fn clean_string(dirty_string: &str) -> String {
    let stripped_string: String = strip_punctuation(&dirty_string);

    let transliterated_str: String = unidecode::unidecode(&stripped_string);

    transliterated_str.to_ascii_lowercase()
}

/// Strip all punctuation except apostrophe
fn strip_punctuation(string: &str) -> String {
    let is_unwanted_punctuation = |character: char| character.is_ascii_punctuation() && character != '\''; 

    string.chars().filter(|&c| !is_unwanted_punctuation(c)).collect()
}

/// Find the term frequency for every n-gram up to words_in_n_gram
/// 
/// This isn't even close to an optimal implementation, especially in terms
/// of space complexity.
pub fn n_gram_frequency_map_from_string(words_in_n_gram: u8, document: &str) -> HashMap<String, u64> {
    assert!(words_in_n_gram > 0, "Panicked asserting the smallest N-grams counted are 1-grams");

    let mut n_gram_map: HashMap<String, u64> = HashMap::new();

    for i in 1..(words_in_n_gram + 1) {
        if i == 1 {
            for word in clean_string(document).split_whitespace() {
                let count = n_gram_map.entry(word.to_string()).or_insert(0);
                *count += 1;
            }

            continue;
        } else {
            let cleaned_string = clean_string(document);
            let grams: Vec<_> = cleaned_string.split_whitespace().ngrams(i as usize).collect();
            for gram_vec in grams {
                let gram_string: String = gram_vec.join(" ");
                let count = n_gram_map.entry(gram_string).or_insert(0);
                *count += 1;
            }
        }
    }
    
    n_gram_map
}

#[derive(Debug, Deserialize)]
struct TermIDF {
    term: String,
    idf: Option<f64>,
}

/// Construct a new map from terms to IDFs for those terms
/// 
/// IDF stands for inverse document frequency, which can be used to weight
/// terms in text analysis (e.g. in the form of TF-IDF)
/// 
/// It's very costly to compute IDF, so they should be computed ahead of time
pub fn get_idf_from_file(filename: &str) -> HashMap<String, f64> {
    let idf_file_contents = fs::File::open(filename).expect("Something went wrong reading the IDF file");
    let mut map: HashMap<String, f64> = HashMap::new();
    let mut reader = csv::Reader::from_reader(idf_file_contents);

    for result in reader.deserialize() {
        let record: TermIDF = result.expect("Something went wrong reading in a line");
        let term_idf: f64 = record.idf.expect("Something went wrong reading in an IDF");
        map.insert(record.term, term_idf);
    }

    return map;
}

/// Given a term frequency map and an inverse document frequency map, generate
/// a TF-IDF map by multiplying each term's frequency by its IDF.
/// 
/// Note that the return value can be thought of as an N-dimensional vector,
/// where each term represents a different dimension.
pub fn get_tf_idf_map(tf_map: &HashMap<String, u64>, idf_map: &HashMap<String, f64>) -> HashMap<String, f64> {
    let mut tf_idf_map: HashMap<String, f64> = HashMap::new();

    for term in tf_map.keys() {
        if !(tf_map.contains_key(term) && idf_map.contains_key(term)) {
            continue;
        }

        let term_frequency: f64 = tf_map[term] as f64;
        let idf: f64 = idf_map[term];
        tf_idf_map.insert(term.to_string(), term_frequency * idf);
    }

    tf_idf_map
}